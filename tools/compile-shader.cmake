#--------------------------------------------------------
#    ASD cmake shader compiler wrapper
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.0)

#--------------------------------------------------------

get_filename_component(OUTPUT_NAME ${OUTPUT_FILE} NAME_WE)
get_filename_component(OUTPUT_DIR ${OUTPUT_FILE} DIRECTORY)

set(CHECKSUM_FILE ${OUTPUT_DIR}/${OUTPUT_NAME}.sha1)

file(SHA1 ${INPUT} INPUT_CHECKSUM)
file(SHA1 ${CMAKE_CURRENT_LIST_FILE} SCRIPT_CHECKSUM)

set(CHECKSUM "${INPUT_CHECKSUM}\n${SCRIPT_CHECKSUM}")

if (NOT EXISTS ${OUTPUT_FILE})
    if (NOT EXISTS ${OUTPUT_DIR})
        file(MAKE_DIRECTORY ${OUTPUT_DIR})
    endif()

    file(WRITE ${OUTPUT_FILE})
elseif(EXISTS ${CHECKSUM_FILE})
    file(READ ${CHECKSUM_FILE} SAVED_CHECKSUM)

    if (${SAVED_CHECKSUM} STREQUAL ${CHECKSUM})
        return()
    endif()
endif()

set(VARIABLE ${OUTPUT_VARIABLE}_${SHADER_TYPE})

file(READ ${INPUT} CONTENTS)
set(INCLUDE "#include <opengl/vertex_layout.h>")

if ("${SHADER_TYPE}" STREQUAL "vs")
    string(REGEX MATCH "!vertex:[ a-z2-4-]*" layout "${CONTENTS}")

    if (NOT "${layout}" STREQUAL "")
        string(REGEX REPLACE "!vertex:[ ]*" "" elements ${layout})
        string(REGEX REPLACE " " ", ::asd::gfx::vertex_attributes::" elements ${elements})

        set(LAYOUT "using ${OUTPUT_VARIABLE}_layout = ::asd::gfx::vertex_layouts::generator<::asd::gfx::vertex_attributes::${elements}>;")
    else()
        set(LAYOUT "#error \"The vertex layout must be declared in the shader code! Example: /* !vertex: p3 c4 */\"\n")
    endif()

    configure_file("${CMAKE_CURRENT_LIST_DIR}/templates/shader.vs.h" ${OUTPUT_FILE})
elseif("${SHADER_TYPE}" STREQUAL "fs")
    set(sampler_slots)

    string(REPLACE "\n" ";" lines "${CONTENTS}")

    foreach (SEGMENT ${lines})
        if ("${SEGMENT}" MATCHES "^[ \t\r]*uniform[ \t\r]+sampler2D[ \t\r]+(.+)$")
            set(sampler_slots ${sampler_slots} ${CMAKE_MATCH_1})
        endif()
    endforeach()

    if (sampler_slots)
        string(REPLACE ";" "\", \"" sampler_slots "\"${sampler_slots}\"")
        set(SAMPLER_SLOTS "constexpr std::string_view ${OUTPUT_VARIABLE}_sampler_slots[] = {${sampler_slots}};")
    else()
        set(SAMPLER_SLOTS "constexpr std::nullptr_t ${OUTPUT_VARIABLE}_sampler_slots = nullptr;")
    endif()

    configure_file("${CMAKE_CURRENT_LIST_DIR}/templates/shader.fs.h" ${OUTPUT_FILE})
else()
    configure_file("${CMAKE_CURRENT_LIST_DIR}/templates/shader.h" ${OUTPUT_FILE})
endif()

message("embedded glsl shader save succeeded; see ${OUTPUT_FILE}")

file(WRITE ${CHECKSUM_FILE} "${CHECKSUM}")

#--------------------------------------------------------
