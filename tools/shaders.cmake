#--------------------------------------------------------
#    ASD cmake shader facilities
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.3)

#--------------------------------------------------------

set(ASD_OPENGL_TOOLS_PATH "${CMAKE_CURRENT_LIST_DIR}" CACHE PATH "asd.opengl tools path" FORCE)

set(USE_OPENGL ON)
set(OPENGL_SHADER_TYPE GLSL)

if ("${CMAKE_SYSTEM_NAME}" MATCHES "^Emscripten|Android|iOS$")
    set(OPENGL_SHADER_TYPE GLSL_ES)
endif()

set(SUPPORTED_SHADER_TYPES GLSL GLSL_ES)

function(shaders TYPE OUTPUT)
    collect_files(shaders_list ${ARGN})
    module_register_files(${shaders_list})

    if (NOT "${TYPE}" STREQUAL "${OPENGL_SHADER_TYPE}")
        if (NOT "${TYPE}" IN_LIST SUPPORTED_SHADER_TYPES)
            message(FATAL_ERROR "Incorrect shader type: ${TYPE}")
        endif()

        return()
    endif()

    set(CURRENT_SHADERS_LIST)
    set(OUTPUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}/generated/include")

    if (NOT EXISTS "${OUTPUT_DIR}")
        file(MAKE_DIRECTORY "${OUTPUT_DIR}")
    endif()

    module_include_directories("${OUTPUT_DIR}")

    foreach(shader ${shaders_list})
        get_filename_component(FILE_NAME "${shader}" NAME_WLE)
        get_filename_component(FILE_DIR "${shader}" DIRECTORY)

        if ("${CURRENT_SOURCE_GROUP_PATH}" STREQUAL "")
            string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}" "" SHADER_PATH "${FILE_DIR}")
        else()
            string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}/${CURRENT_SOURCE_GROUP_PATH}" "" SHADER_PATH "${FILE_DIR}")
        endif()

        set(INPUT_FILE "${shader}")  # Input shader
        set(OUTPUT_FILE "${OUTPUT_DIR}/${SHADER_PATH}/${FILE_NAME}.shader.h") # Output .shader.h file

        # Get type of shader to compile (vs, ps/fs)

        string(LENGTH "${FILE_NAME}" FILE_LENGTH)
        math(EXPR FILE_LENGTH "${FILE_LENGTH} - 2")
        string(SUBSTRING "${FILE_NAME}" ${FILE_LENGTH} -1 SHADER_TYPE)
        math(EXPR FILE_LENGTH "${FILE_LENGTH} - 1")
        string(SUBSTRING "${FILE_NAME}" 0 ${FILE_LENGTH} SHADER_ID)
        string(TOLOWER "${SHADER_TYPE}" SHADER_TYPE)

        # Add the command that will process each single file

        string(REGEX REPLACE "^[\\/]+" "" SHADER_PATH "${SHADER_PATH}")

        if (NOT "${CURRENT_SOURCE_DOMAIN}" STREQUAL "")
            set(SHADER_BASE_PATH "${CURRENT_SOURCE_DOMAIN}/")
        endif()

        if (NOT "${SHADER_BASE_PATH}" STREQUAL "")
            string(REPLACE "${SHADER_BASE_PATH}" "" SHADER_ID "${SHADER_PATH}/${SHADER_ID}")
        else()
            set(SHADER_ID "${SHADER_PATH}/${SHADER_ID}")
        endif()

        string(REGEX REPLACE "[\\/]" _ SHADER_GROUP "${SHADER_ID}")
        string(REGEX REPLACE "[^a-zA-Z0-9_]" "" SHADER_GROUP "${SHADER_GROUP}")

        set(OUTPUT_VARIABLE shader_code_${SHADER_GROUP})

        if (NOT ";${CURRENT_SHADERS_LIST};" MATCHES ";${SHADER_GROUP};")
            set(CURRENT_SHADERS_LIST ${CURRENT_SHADERS_LIST} "${SHADER_GROUP}")
            set(${SHADER_GROUP}_id "${SHADER_ID}" CACHE INTERNAL "" FORCE)
            set(${SHADER_GROUP}_path "${SHADER_BASE_PATH}" CACHE INTERNAL "" FORCE)
        endif()

        if (NOT ";${${SHADER_GROUP}_types};" MATCHES ";${SHADER_TYPE};")
            set(${SHADER_GROUP}_types ${${SHADER_GROUP}_types} ${SHADER_TYPE} CACHE INTERNAL "" FORCE)
        endif()

        add_custom_command(
            OUTPUT ${OUTPUT_FILE}
            COMMAND ${CMAKE_COMMAND} "-DINPUT=${INPUT_FILE}" "-DOUTPUT_FILE=${OUTPUT_FILE}" "-DSHADER_TYPE=${SHADER_TYPE}" "-DOUTPUT_VARIABLE=${OUTPUT_VARIABLE}" -P "${ASD_OPENGL_TOOLS_PATH}/compile-shader.cmake"
            MAIN_DEPENDENCY "${INPUT_FILE}"
            DEPENDS "${ASD_OPENGL_TOOLS_PATH}/compile-shader.cmake"
            WORKING_DIRECTORY "${ASD_OPENGL_TOOLS_PATH}"
            VERBATIM
        )
    endforeach()

    set(${OUTPUT} ${CURRENT_SHADERS_LIST} PARENT_SCOPE)
endfunction()

function(shaders_output namespace filename)
    color_message("   Configure output shader file: " BOLD "${filename}" RESET "...")

    set(TEMPLATE_FILE "${ASD_OPENGL_TOOLS_PATH}/templates/embedded.h")
    set(INCLUDES)

    foreach(SHADER_GROUP ${ARGN})
        set(SHADER_BASE_PATH ${${SHADER_GROUP}_path})
        set(SHADER_ID ${${SHADER_GROUP}_id})

        set(SHADER_CODE_UNITS)

        foreach(SHADER_TYPE ${${SHADER_GROUP}_types})
            if ("${SHADER_TYPE}" STREQUAL "vs")
                set(NATIVE_SHADER_TYPE GL_VERTEX_SHADER)
            elseif("${SHADER_TYPE}" STREQUAL "fs")
                set(NATIVE_SHADER_TYPE GL_FRAGMENT_SHADER)
            elseif("${SHADER_TYPE}" STREQUAL "gs")
                set(NATIVE_SHADER_TYPE GL_GEOMETRY_SHADER)
            endif()

            set(INCLUDES "${INCLUDES}#include <${SHADER_BASE_PATH}${SHADER_ID}.${SHADER_TYPE}.shader.h>\n")

            if ("${SHADER_CODE_UNITS}" STREQUAL "")
                set(SHADER_CODE_UNITS "{shader_code_${SHADER_GROUP}_${SHADER_TYPE}, ${NATIVE_SHADER_TYPE}}")
            else()
                set(SHADER_CODE_UNITS "${SHADER_CODE_UNITS}, {shader_code_${SHADER_GROUP}_${SHADER_TYPE}, ${NATIVE_SHADER_TYPE}}")
            endif()
        endforeach()

        unset(${SHADER_GROUP}_id CACHE)
        unset(${SHADER_GROUP}_path CACHE)
        unset(${SHADER_GROUP}_types CACHE)

        set(SHADER_CODE_UNITS "    constexpr shader_code_unit shader_code_${SHADER_GROUP}[] = {${SHADER_CODE_UNITS}};\n")

        if ("${CONTENTS}" STREQUAL "")
            set(CONTENTS "${SHADER_CODE_UNITS}")
        else()
            set(CONTENTS "${CONTENTS}${SHADER_CODE_UNITS}")
        endif()

        set(ShaderSet "        {\"${SHADER_ID}\", shader_code{shader_code_${SHADER_GROUP}, shader_code_${SHADER_GROUP}_layout::instance, shader_code_${SHADER_GROUP}_sampler_slots}}")

        if ("${SHADER_SET_ARRAY}" STREQUAL "")
            set(SHADER_SET_ARRAY "${ShaderSet}")
        else()
            set(SHADER_SET_ARRAY "${SHADER_SET_ARRAY},\n${ShaderSet}")
        endif()
    endforeach()

    set(CONTENTS "${CONTENTS}\n    constexpr auto shaders = ::asd::meta::make_map<std::string_view, shader_code>({\n${SHADER_SET_ARRAY}\n    });")

    set(outfile "${CMAKE_CURRENT_SOURCE_DIR}/generated/${CURRENT_SOURCE_GROUP_PATH}/${CURRENT_SOURCE_DOMAIN}/${filename}")
    configure_file(${TEMPLATE_FILE} "${outfile}")
    module_include_directories("${CMAKE_CURRENT_SOURCE_DIR}/generated/${CURRENT_SOURCE_GROUP_PATH}")
endfunction()
