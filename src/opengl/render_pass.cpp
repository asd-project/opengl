//---------------------------------------------------------------------------

#include <opengl/render_pass.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        render_pass<opengl::graphics>::render_pass(
            const gfx::render_pass_data<opengl::graphics> & data,
            opengl::graphics & graphics
        ) :
            _data(data),
            _graphics(&graphics)
        {}

        void render_pass<opengl::graphics>::start() {
            _graphics->set_blending(_data.blending);

            GLbitfield clear_bits = GL_COLOR_BUFFER_BIT;
            glClearColor(_data.clear_color.r, _data.clear_color.g, _data.clear_color.b, _data.clear_color.a);

            if (_data.features.check(gfx::feature::depth_test)) {
                glEnable(GL_DEPTH_TEST);
                glClearDepth(_data.clear_depth);
                clear_bits |= GL_DEPTH_BUFFER_BIT;
            } else {
                glDisable(GL_DEPTH_TEST);
            }

            if (_data.features.check(gfx::feature::stencil_test)) {
                glEnable(GL_STENCIL_TEST);
                glClearDepth(_data.clear_stencil);
                clear_bits |= GL_STENCIL_BUFFER_BIT;
            } else {
                glDisable(GL_STENCIL_TEST);
            }

            glBindFramebuffer(GL_FRAMEBUFFER, _data.target ? _data.target->framebuffer() : 0);
            glClear(clear_bits);
        }

        void render_pass<opengl::graphics>::end() {
            if (_data.target) {
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
            }
        }
    }
}

//---------------------------------------------------------------------------
