//---------------------------------------------------------------------------

#include <opengl/multimesh.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        multimesh_buffer::multimesh_buffer(size_t max_size) :
            data(max_size) {
            glGenBuffers(1, &handle);
        }

        multimesh_buffer::~multimesh_buffer() {
            if (handle > 0) {
                glDeleteBuffers(1, &handle);
            }
        }

        ptrdiff_t multimesh_buffer::get_offset(const span<std::byte> & chunk) const {
            return chunk.data() - data.data();
        }

        void plain_multimesh_drawer::render(gfx::multimesh_scheme<opengl::graphics> & mesh) {
            glBindVertexArray(mesh.handle());
            OPENGL_TRY(glDrawArraysInstanced(mesh.primitives_type(), static_cast<GLint>(0), mesh.vertices_count(), mesh.instances_count()));
            glBindVertexArray(0);
        }

        void indexed_multimesh_drawer::render(gfx::multimesh_scheme<opengl::graphics> & mesh) {
            glBindVertexArray(mesh.handle());
            OPENGL_TRY(glDrawElementsInstanced(mesh.primitives_type(), buffer.indices_count, buffer.indices_type, nullptr, mesh.instances_count()));
            glBindVertexArray(0);
        }
    }

    namespace gfx
    {
        using namespace std::string_literals;

        multimesh_scheme<opengl::graphics>::multimesh_scheme(const gfx::multimesh_scheme_data<opengl::graphics> & data) :
            opengl::basic_mesh(data.layout, data.primitives_type, data.visible_face),
            _instance_layout(&data.instance_layout),
            _instance_buffer(65536),
            _drawer(opengl::plain_multimesh_drawer{})
        {
            glBindVertexArray(_handle);
            glBindBuffer(GL_ARRAY_BUFFER, _instance_buffer.handle);
            glBufferData(GL_ARRAY_BUFFER, _instance_buffer.data.size(), _instance_buffer.data.data(), GL_STREAM_DRAW);

            GLint attr_count = 0;

            auto attributes = _layout->attributes();
            for (auto & attribute : attributes) {
                attr_count += (attribute.units - 1) / 4 + 1;
            }

            std::byte * pointer = nullptr;

            for (auto & attribute : _instance_layout->attributes()) {
                size_t count = static_cast<size_t>((attribute.units - 1) / 4 + 1);

                auto type = opengl::transform_type(attribute.type);
                auto unit_size = attribute.size_in_bytes / attribute.units;

                for (size_t j = 0; j < count; ++j, ++attr_count) {
                    GLint u = (j == count - 1) ? ((attribute.units - 1) % 4) + 1 : 4;
                    glEnableVertexAttribArray(attr_count);
                    glVertexAttribPointer(attr_count, u, type, GL_FALSE, static_cast<GLsizei>(_instance_layout->size_in_bytes()), pointer);
                    glVertexAttribDivisor(attr_count, 1);
                    pointer += u * unit_size;
                }
            }

            glBindVertexArray(0);
        }

        multimesh_data multimesh_scheme<opengl::graphics>::instance() {
            if (_free_list.size() > 0) {
                auto entry = _free_list.back();
                _free_list.pop_back();
                return entry;
            }

            ++_instances_count;

            while (_instance_buffer.data.size() < _instance_buffer.offset + block_size()) {
                _dirty = true;
                auto size = _instance_buffer.data.size();
                _instance_buffer.data.resize(size * 2);
                memset(_instance_buffer.data.data() + size, 0, _instance_buffer.data.size() - size);

                OPENGL_TRY(glBindBuffer(GL_ARRAY_BUFFER, _instance_buffer.handle));
                OPENGL_TRY(glBufferData(GL_ARRAY_BUFFER, _instance_buffer.data.size(), _instance_buffer.data.data(), GL_STREAM_DRAW));
            }

            auto offset = _instance_buffer.offset;
            _instance_buffer.offset += block_size();

            return { offset, block_size() };
        }

        void * multimesh_scheme<opengl::graphics>::map_attribute(const multimesh_data & instance, ptrdiff_t offset) {
            return _instance_buffer.data.data() + instance.pointer + offset;
        }

        void multimesh_scheme<opengl::graphics>::free(const multimesh_data & instance) {
            if (instance.size == 0) {
                return;
            }

            _dirty = true;
            std::memset(_instance_buffer.data.data() + instance.pointer, 0, instance.size);
            _free_list.push_back(instance);
        }

        void multimesh_scheme<opengl::graphics>::render() {
#if GL_DEBUG
            const auto attributes = _layout->attributes();

            if (_registered_attributes.size() < attributes.size()) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Mesh is incomplete, there are missing layout attributes"));
            }
#endif

            sync();
            std::visit([this](auto & drawer) { drawer.render(*this); }, _drawer);
        }

        void multimesh_scheme<opengl::graphics>::sync() {
            if (!_dirty) {
                return;
            }

            OPENGL_TRY(glBindBuffer(GL_ARRAY_BUFFER, _instance_buffer.handle));
            OPENGL_TRY(glBufferSubData(GL_ARRAY_BUFFER, 0, _instance_buffer.data.size(), _instance_buffer.data.data()));

            _dirty = false;
        }

        void multimesh_scheme<opengl::graphics>::set_indices(const span<const void> & indices, gfx::index_type type) {
            auto v = overloaded{
                [&] (opengl::indexed_multimesh_drawer & drawer) {
                    if (!indices.empty()) {
                        drawer.buffer.update(indices, type);
                        return;
                    }

                    OPENGL_TRY(glBindVertexArray(_handle));
                    OPENGL_TRY(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
                    OPENGL_TRY(glBindVertexArray(0));

                    _drawer.emplace<opengl::plain_multimesh_drawer>();
                },

                [&] (opengl::plain_multimesh_drawer &) {
                    if (indices.empty()) {
                        return;
                    }

                    auto & drawer = _drawer.emplace<opengl::indexed_multimesh_drawer>();

                    OPENGL_TRY(glBindVertexArray(_handle));
                    drawer.buffer.update(indices, type);
                    OPENGL_TRY(glBindVertexArray(0));
                }
            };

            std::visit(v, _drawer);
        }
    }
}

//---------------------------------------------------------------------------
