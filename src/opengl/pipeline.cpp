//---------------------------------------------------------------------------

#include <opengl/pipeline.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
#if !ASD_OPENGL_ES
        static GLenum convert_polygon_mode(gfx::polygon_mode mode) {
            switch (mode) {
                case gfx::polygon_mode::fill:
                    return GL_FILL;

                case gfx::polygon_mode::wireframe:
                    return GL_LINE;

                default:
                    return GL_FILL;
            }
        }
#endif
    }

    namespace gfx
    {
        pipeline<opengl::graphics>::pipeline(const gfx::pipeline_data<opengl::graphics> & data) :
            _program(data.program)
#if !ASD_OPENGL_ES
            , _polygon_mode(opengl::convert_polygon_mode(data.polygon_mode))
#endif
            {}

        void pipeline<opengl::graphics>::activate() {
#if !ASD_OPENGL_ES
            glPolygonMode(GL_FRONT_AND_BACK, _polygon_mode);
#endif

            _program.activate();
        }
    }
}

//---------------------------------------------------------------------------
