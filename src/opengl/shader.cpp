//---------------------------------------------------------------------------

#include <opengl/shader.h>
#include <opengl/shaders/embedded.h>
#include <opengl/uniform.h>

#include <set>

#include <spdlog/spdlog.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        const GLchar * char_cast(const char * s) {
            return static_cast<const GLchar *>(static_cast<const void *>(s));
        }
    }

    namespace gfx
    {
        using namespace std::string_literals;

        shader_program<opengl::graphics>::shader_program(std::string_view name, const gfx::shader_code & code, gfx::uniform_registry<opengl::graphics> & uniforms) :
            _program_id(OPENGL_TRY_AND_RETURN(glCreateProgram())),
            _layout(code.layout)
        {
            GLint status = GL_FALSE;

            std::vector<uint32> ids(code.units.size());

            for(auto & unit : code.units) {
                auto shader_id = OPENGL_TRY_AND_RETURN(glCreateShader(unit.type));
                ids.push_back(shader_id);

                const GLchar * code[] = { opengl::char_cast(unit.code) };

                OPENGL_TRY(glShaderSource(shader_id, 1, code, nullptr));
                OPENGL_TRY(glCompileShader(shader_id));

                OPENGL_TRY(glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status));

                if (status == GL_FALSE) {
                    int info_log_length = 0;
                    glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_length);

                    if (info_log_length != 0) {
                        std::vector<char> buffer(static_cast<size_t>(info_log_length + 1));

                        glGetShaderInfoLog(shader_id, info_log_length, nullptr, buffer.data());
                        buffer[info_log_length] = '\0';

                        spdlog::error("[shader_program] GLSL {} compilation error ({}):\n{}", (unit.type == GL_VERTEX_SHADER ? "vertex shader" : "fragment shader"), name, buffer.data());
                    }

                    BOOST_THROW_EXCEPTION(std::runtime_error("[shader_program] Can't compile GLSL "s + (unit.type == GL_VERTEX_SHADER ? "vertex shader" : "fragment shader") + ": " + name));
                }

                glAttachShader(_program_id, shader_id);
            }

            uint32 k = 0;

            for(auto & attribute : code.layout->attributes()) {
                glBindAttribLocation(_program_id, k, attribute.name);

                if (attribute.alias) {
                    glBindAttribLocation(_program_id, k, attribute.alias);
                }

                k += (attribute.units - 1) / 4 + 1;
            }

            glLinkProgram(_program_id);

            OPENGL_TRY(glGetProgramiv(_program_id, GL_LINK_STATUS, &status));

            if (status == GL_FALSE) {
                int info_log_length = 0;
                glGetProgramiv(_program_id, GL_INFO_LOG_LENGTH, &info_log_length);

                if (info_log_length != 0) {
                    std::vector<char> buffer(static_cast<size_t>(info_log_length + 1));

                    glGetProgramInfoLog(_program_id, info_log_length, nullptr, buffer.data());
                    buffer[info_log_length] = '\0';

                    spdlog::error("[shader_program] GLSL shader program linking error ({}):\n{}", name, buffer.data());
                }

                BOOST_THROW_EXCEPTION(std::runtime_error("[shader_program] Can't link GLSL shader program: "s + name));
            }

#if GL_DEBUG
            {
                GLint attributes_count = 0;
                GLint max_attr_length = 0;

                glGetProgramiv(_program_id, GL_ACTIVE_ATTRIBUTES, &attributes_count);
                glGetProgramiv(_program_id, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &max_attr_length);

                std::string buffer(max_attr_length + 1, 0);
                std::set<std::string> attributes;

                for (int i = 0; i < attributes_count; ++i) {
                    GLenum type = 0;
                    GLint length = 0;
                    GLint size = 0;
                    glGetActiveAttrib(_program_id, i, max_attr_length, &length, &size, &type, buffer.data());
                    attributes.insert({buffer.begin(), buffer.begin() + length});
                }

                for (auto & attribute : code.layout->attributes()) {
                    if (attributes.find(attribute.name) == attributes.end()) {
                        spdlog::warn("[shader_program] {}: attribute \"{}\" is missing, may be optimized away", name, attribute.name);
                    }
                }
            }
#endif

            GLint blocks_count = 0;

            glGetProgramiv(_program_id, GL_ACTIVE_UNIFORM_BLOCKS, &blocks_count);

            GLint length = 0;

            for (GLint i = 0; i < blocks_count; i++) {
                glGetActiveUniformBlockiv(_program_id, i, GL_UNIFORM_BLOCK_NAME_LENGTH, &length);
                std::vector<GLchar> uniform_name(length, '\0');
                glGetActiveUniformBlockName(_program_id, i, length, nullptr, &uniform_name[0]);

                auto * uniform = uniforms.find_scheme(uniform_name.data());

                if (!uniform) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("[shader_program] Couldn't find registered uniform with the given name: " + std::string(uniform_name.data())));
                }

                _uniforms.push_back(uniform);
                glUniformBlockBinding(_program_id, i, uniform->id());
            }

            for (auto & shader_id : ids) {
                glDeleteShader(shader_id);
            }

            glUseProgram(_program_id);

            // bind texture channels
            for (int i = 0; i < static_cast<int>(code.sampler_slots.size()); ++i) {
                std::string name {code.sampler_slots[i]};
                GLint location = glGetUniformLocation(_program_id, name.c_str());

                if (location >= 0) {
                    glUniform1i(location, i);
                }
            }

            glUseProgram(0);
        }

        shader_program<opengl::graphics>::shader_program(shader_program && program) noexcept :
            _program_id(std::exchange(program._program_id, 0)),
            _uniforms(std::move(program._uniforms))
        {}

        shader_program<opengl::graphics>::~shader_program() {
            if (_program_id != 0) {
                glDeleteProgram(_program_id);
            }
        }

        void shader_program<opengl::graphics>::activate() {
            for (auto & uniform : _uniforms) {
                uniform->sync();
            }

            glUseProgram(_program_id);
        }
    }
}

//---------------------------------------------------------------------------
