//---------------------------------------------------------------------------

#include <opengl/texture.h>
#include "opengl/opengl.h"

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        enum pixel_alignment : GLint
        {
            byte = 1,
            double_byte = 2,
            word = 4,
            double_word = 8
        };

        struct format_parameters
        {
            GLenum input_format;
            GLenum type;
            GLint alignment;
        };

        GLenum get_internal_format(const gfx::texture_data<opengl::graphics> & data) {
            if (data.internal_format != 0) {
                return static_cast<GLenum>(data.internal_format);
            }

            switch (data.format) {
                case gfx::texture_format::grayscale:
                    return GL_R8;
                case gfx::texture_format::grayscale_alpha:
                    return GL_RG8;
                case gfx::texture_format::rgb:
                    return GL_RGB8;
                case gfx::texture_format::rgba:
                    return GL_RGBA8;
                case gfx::texture_format::bgra:
                    return GL_RGBA8;
                case gfx::texture_format::rgb16f:
                    return GL_RGB16F;
                case gfx::texture_format::rgba16f:
                    return GL_RGBA16F;
                case gfx::texture_format::rgb32f:
                    return GL_RGB32F;
                case gfx::texture_format::rgba32f:
                    return GL_RGBA32F;
            }

            BOOST_ASSERT_MSG(false, "[opengl][get_internal_format] Invalid texture format value");
            return 0;
        }

        GLenum get_internal_filtering(const gfx::texture_data<opengl::graphics> & data) {
            switch (data.filtering) {
                case gfx::texture_filtering::nearest:
                    return GL_NEAREST;
                case gfx::texture_filtering::linear:
                    return GL_LINEAR;
            }

            BOOST_ASSERT_MSG(false, "[opengl][get_internal_filtering] Invalid texture filtering value");
            return 0;
        }

        format_parameters get_format_parameters(const gfx::texture_data<opengl::graphics> & data) {
            switch (data.format) {
                case gfx::texture_format::grayscale:
                    return {GL_RED, GL_UNSIGNED_BYTE, pixel_alignment::byte};
                case gfx::texture_format::grayscale_alpha:
                    return {GL_RG, GL_UNSIGNED_BYTE, pixel_alignment::double_byte};
                case gfx::texture_format::rgb:
                    return {GL_RGB, GL_UNSIGNED_BYTE, pixel_alignment::byte};
                case gfx::texture_format::rgba:
                    return {GL_RGBA, GL_UNSIGNED_BYTE, pixel_alignment::word};
                case gfx::texture_format::bgra:
                    return {GL_BGRA, GL_UNSIGNED_BYTE, pixel_alignment::word};
                case gfx::texture_format::rgb16f:
                    return {GL_RGB, GL_HALF_FLOAT, pixel_alignment::double_byte};
                case gfx::texture_format::rgba16f:
                    return {GL_RGBA, GL_HALF_FLOAT, pixel_alignment::double_byte};
                case gfx::texture_format::rgb32f:
                    return {GL_RGB, GL_FLOAT, pixel_alignment::word};
                case gfx::texture_format::rgba32f:
                    return {GL_RGBA, GL_FLOAT, pixel_alignment::word};
            }

            BOOST_ASSERT_MSG(false, "[opengl][get_format_parameters] Invalid texture format value");
            return {};
        }
    }

    namespace gfx
    {
        texture<opengl::graphics>::texture(const gfx::texture_data<opengl::graphics> & data, GLenum target) :
            _target(target)
        {
            OPENGL_TRY(glGenTextures(1, &_id));
            update(data);
        }

        texture<opengl::graphics>::~texture() {
            if (_id.valid()) {
                glDeleteTextures(1, &_id);
            }
        }

        void texture<opengl::graphics>::update(const gfx::texture_data<opengl::graphics> & data) {
            BOOST_ASSERT_MSG(data.pixels.empty() || data.pixels.size() >= data.size.area() * bytes_per_pixel(data.format), "Pixels buffer is too small");

            _data = data;

            glBindTexture(_target, _id);

            auto internal_format = opengl::get_internal_format(_data);
            auto params = opengl::get_format_parameters(_data);
            auto internal_filtering = opengl::get_internal_filtering(_data);

            // OPENGL_TRY(glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE));
            // OPENGL_TRY(glPixelStorei(GL_UNPACK_LSB_FIRST, GL_FALSE));
            // OPENGL_TRY(glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, 0));
            // OPENGL_TRY(glPixelStorei(GL_UNPACK_SKIP_ROWS, 0));
            // OPENGL_TRY(glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0));
            // OPENGL_TRY(glPixelStorei(GL_UNPACK_SKIP_IMAGES, 0));
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ROW_LENGTH, _data.size.x));
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ALIGNMENT, params.alignment));

            OPENGL_TRY(glTexImage2D(_target, 0, internal_format, _data.size.x, _data.size.y, 0, params.input_format, params.type, _data.pixels.data()));
            OPENGL_TRY(glTexParameteri(_target, GL_TEXTURE_MAG_FILTER, internal_filtering));
            OPENGL_TRY(glTexParameteri(_target, GL_TEXTURE_MIN_FILTER, internal_filtering));
            OPENGL_TRY(glTexParameteri(_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
            OPENGL_TRY(glTexParameteri(_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

            _data.internal_format = internal_format;
            _data.pixels.reset();
        }

        void texture<opengl::graphics>::update_pixels(pixel_data pixels) {
            BOOST_ASSERT_MSG(!pixels.empty(), "Pixels buffer should not be empty");
            BOOST_ASSERT_MSG(pixels.size() >= size_in_bytes(), "Pixels buffer is too small");

            auto params = opengl::get_format_parameters(_data);
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ROW_LENGTH, _data.size.x));
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ALIGNMENT, params.alignment));
            OPENGL_TRY(glTexSubImage2D(_target, 0, 0, 0, _data.size.x, _data.size.y, params.input_format, params.type, pixels.data()));
        }

        void texture<opengl::graphics>::update_region(const math::uint_rect & region, pixel_data pixels) {
            BOOST_ASSERT_MSG(!pixels.empty(), "Pixels buffer should not be empty");
            BOOST_ASSERT_MSG(pixels.size() >= region.area() * bytes_per_pixel(_data.format), "Pixels buffer is too small");
            BOOST_ASSERT_MSG(_data.size.contains(region.max), "Updated region should not intersect texture bounds");

            auto params = opengl::get_format_parameters(_data);
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ROW_LENGTH, 0));
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ALIGNMENT, params.alignment));
            OPENGL_TRY(glTexSubImage2D(_target, 0, region.left, region.top, region.width(), region.height(), params.input_format, params.type, pixels.data()));
        }

        void texture<opengl::graphics>::set_filtering(texture_filtering filtering) {
            if (_data.filtering == filtering) {
                return;
            }

            _data.filtering = filtering;

            glBindTexture(_target, _id);

            auto internal_filtering = opengl::get_internal_filtering(_data);
            glTexParameteri(_target, GL_TEXTURE_MAG_FILTER, internal_filtering);
            glTexParameteri(_target, GL_TEXTURE_MIN_FILTER, internal_filtering);
        }

        void texture<opengl::graphics>::resize(const math::uint_size & size, area_snapping snapping) {
            if (_data.size == size) {
                return;
            }

            glBindTexture(_target, _id);

            auto bpp = bytes_per_pixel(_data.format);
            auto params = opengl::get_format_parameters(_data);

            std::vector<std::byte> old_pixels{_data.size.area() * bpp};
            OPENGL_TRY(glPixelStorei(GL_PACK_ALIGNMENT, params.alignment));
            OPENGL_TRY(glGetTexImage(_target, 0, params.input_format, params.type, old_pixels.data()));

            std::vector<std::byte> new_pixels = gfx::copy_pixels(old_pixels, _data.size, size, bpp, snapping);

            _data.size = size;

            auto internal_format = opengl::get_internal_format(_data);
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ROW_LENGTH, _data.size.x));
            OPENGL_TRY(glPixelStorei(GL_UNPACK_ALIGNMENT, params.alignment));
            OPENGL_TRY(glTexImage2D(_target, 0, internal_format, _data.size.x, _data.size.y, 0, params.input_format, params.type, new_pixels.data()));
        }
    }
}

//---------------------------------------------------------------------------
