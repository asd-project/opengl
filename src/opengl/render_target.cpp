//---------------------------------------------------------------------------

#include <iostream>
#include <opengl/render_target.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        render_target<opengl::graphics>::render_target(const render_target_data<opengl::graphics> & data) :
            _size(data.size)
        {
            glGenFramebuffers(1, &_framebuffer);
            OPENGL_TRY(glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer));

            glGenRenderbuffers(1, &_depth_channel);
            OPENGL_TRY(glBindRenderbuffer(GL_RENDERBUFFER, _depth_channel));
            OPENGL_TRY(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, _size.x, _size.y));

            OPENGL_TRY(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depth_channel));

            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Can't set framebuffer"));
            }
        }

        render_target<opengl::graphics>::~render_target() {
            _color_channels.clear();

            if (_framebuffer != 0) {
                glDeleteRenderbuffers(1, &_depth_channel);
                glDeleteFramebuffers(1, &_framebuffer);
            }
        }

        render_target<opengl::graphics> & render_target<opengl::graphics>::operator=(render_target && t) {
            std::swap(_framebuffer, t._framebuffer);
            std::swap(_attachments, t._attachments);
            std::swap(_color_channels, t._color_channels);
            std::swap(_depth_channel, t._depth_channel);
            std::swap(_size, t._size);

            return *this;
        }

        GLuint render_target<opengl::graphics>::framebuffer() const {
            return _framebuffer;
        }

        void render_target<opengl::graphics>::resize(const math::uint_size & size) {
            if (_size == size) {
                return;
            }

            _size = size;
            update_size();
        }

        gfx::texture<opengl::graphics> & render_target<opengl::graphics>::add_buffer(const render_target_buffer_data<opengl::graphics> & buffer_data) {
            gfx::texture_data<opengl::graphics> texture_data;

            texture_data.size = _size;
            texture_data.format = buffer_data.format;
            texture_data.filtering = buffer_data.filtering;

            const auto & channel = *_color_channels.emplace(_color_channels.end(), texture_data, GL_TEXTURE_2D);

            auto attachment = static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + _color_channels.size() - 1);
            glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, channel.target(), channel.id(), 0);

            _attachments.push_back(attachment);
            glDrawBuffers(static_cast<int>(_attachments.size()), _attachments.data());

            return _color_channels.back();
        }

        gfx::texture<opengl::graphics> & render_target<opengl::graphics>::get_buffer(int32 index) {
            return _color_channels[index];
        }

        void render_target<opengl::graphics>::update_size() {
            glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);

            gfx::texture_data<opengl::graphics> data;
            data.size = _size;

            for (auto & channel : _color_channels) {
                data.format = channel.format();
                channel.update(data);
            }

            OPENGL_TRY(glBindRenderbuffer(GL_RENDERBUFFER, _depth_channel));
            OPENGL_TRY(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, _size.x, _size.y));
        }
    }
}

//---------------------------------------------------------------------------
