//---------------------------------------------------------------------------

#include <opengl/opengl.h>
#include <opengl/uniform.h>
#include <opengl/shader.h>
#include <opengl/mesh.h>
#include <opengl/multimesh.h>
#include <opengl/render_pass.h>
#include <opengl/pipeline.h>

#include <spdlog/spdlog.h>

#if BOOST_OS_LINUX
    #include <GL/glu.h>
    #include <GL/glx.h>
#endif

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        void graphics::set_cull_face(GLenum cull_face) {
            if (_cull_face == cull_face) {
                return;
            }

            if (_cull_face == GL_NONE) {
                glEnable(GL_CULL_FACE);
            }

            _cull_face = cull_face;

            if (_cull_face == GL_NONE) {
                glDisable(GL_CULL_FACE);
                return;
            }

            glCullFace(cull_face);
        }

        void graphics::set_blending(gfx::blending blending) {
            if (_blending == blending) {
                return;
            }

            if (_blending == gfx::blending::none) {
                glEnable(GL_BLEND);
            }

            _blending = blending;

            switch (_blending) {
                case gfx::blending::none:
                    glDisable(GL_BLEND);
                    break;
                case gfx::blending::interpolative:
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    break;
                case gfx::blending::additive:
                    glBlendFunc(GL_ONE, GL_ONE);
                    break;
                case gfx::blending::multiplicative:
                    glBlendFunc(GL_DST_COLOR, GL_ZERO);
                    break;
            }
        }

        void graphics::check_for_errors(const char * context) {
            auto error = glGetError();

            if (error != GL_NO_ERROR) {
                auto s = fmt::format("opengl error: {}, code: {:#x}", reinterpret_cast<const char *>(glewGetErrorString(error)), error);
                spdlog::error("{}: {}", context, s);
                BOOST_THROW_EXCEPTION(std::runtime_error(s));
            }
        }

        void GLAPIENTRY glDebugCallbackFunc(uint32 source, uint32 type, uint32 id, uint32 severity, int length, const char * message, const void * userParam) {
            std::string sev, t, src;

            switch (source) {
                case GL_DEBUG_SOURCE_API:
                    src = "API";
                    break;

                case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
                    src = "window system";
                    break;

                case GL_DEBUG_SOURCE_SHADER_COMPILER:
                    src = "shader compiler";
                    break;

                case GL_DEBUG_SOURCE_THIRD_PARTY:
                    src = "3rd party";
                    break;

                case GL_DEBUG_SOURCE_APPLICATION:
                    src = "application";
                    break;

                default: {
                    src = fmt::format("unknown ({:#x})", source);
                    break;
                }
            }

            switch (type) {
                case GL_DEBUG_TYPE_ERROR:
                    spdlog::error("OpenGL error msg (id: {:x}, source: {}): \n{}", id, src, message);
                    return;

                case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
                    t = "deprecated behavior";
                    break;

                case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
                    t = "undefined behavior";
                    break;

                case GL_DEBUG_TYPE_PORTABILITY:
                    t = "portability";
                    break;

                case GL_DEBUG_TYPE_PERFORMANCE:
                    t = "performance";
                    break;

                case GL_DEBUG_TYPE_OTHER:
                    t = "other";
                    break;

                case GL_DEBUG_TYPE_MARKER:
                    t = "marker";
                    break;

                case GL_DEBUG_TYPE_PUSH_GROUP:
                    t = "push group";
                    break;

                case GL_DEBUG_TYPE_POP_GROUP:
                    t = "pop group";
                    break;

                default: {
                    t = fmt::format("unknown ({:#x})", type);
                    break;
                }
            }

            switch (severity) {
                case GL_DEBUG_SEVERITY_LOW:
                    spdlog::debug("OpenGL debug msg (type: {}, severity: notification, id: {:x}, source: {}): \n{}", t, id, src, message);
                    break;

                case GL_DEBUG_SEVERITY_MEDIUM:
                    spdlog::debug("OpenGL debug msg (type: {}, severity: notification, id: {:x}, source: {}): \n{}", t, id, src, message);
                    break;

                case GL_DEBUG_SEVERITY_HIGH:
                    spdlog::debug("OpenGL debug msg (type: {}, severity: high, id: {:x}, source: {}): \n{}", t, id, src, message);
                    break;

                case GL_DEBUG_SEVERITY_NOTIFICATION:
                    spdlog::trace("OpenGL debug msg (type: {}, severity: notification, id: {:x}, source: {}): \n{}", t, id, src, message);
                    break;

                default: {
                    spdlog::trace("OpenGL debug msg (type: {}, severity: {:#x}, id: {:x}, source: {}): \n{}", t, severity, id, src, message);
                    break;
                }
            }

            if (severity == GL_DEBUG_SEVERITY_HIGH) {
                BOOST_THROW_EXCEPTION(std::runtime_error("GL error!"));
            }
        }
    }
}
