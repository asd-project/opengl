//---------------------------------------------------------------------------

#include <iostream>
#include <opengl/uniform.h>
#include <algorithm/align.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        uniform_buffer::uniform_buffer(int idx, size_t max_size) :
            data(max_size) {
            glGenBuffers(1, &handle);

            glBindBuffer(GL_UNIFORM_BUFFER, handle);
            glBufferData(GL_UNIFORM_BUFFER, data.size(), data.data(), GL_STREAM_DRAW);

#if GL_DEBUG
            GLint size;
            glGetBufferParameteriv(GL_UNIFORM_BUFFER, GL_BUFFER_SIZE, &size);
            BOOST_ASSERT(size == static_cast<GLint>(data.size()));
#endif
        }

        uniform_buffer::uniform_buffer(uniform_buffer && buffer) noexcept :
            handle(std::exchange(buffer.handle, 0)),
            offset(std::exchange(buffer.offset, 0)),
            data(std::move(buffer.data)),
            dirty_range(std::move(buffer.dirty_range))
        {}

        uniform_buffer::~uniform_buffer() {
            if (handle > 0) {
                glDeleteBuffers(1, &handle);
            }
        }

        uniform_buffer & uniform_buffer::operator=(uniform_buffer && buffer) noexcept {
            std::swap(handle, buffer.handle);
            std::swap(offset, buffer.offset);
            std::swap(data, buffer.data);
            std::swap(dirty_range, buffer.dirty_range);

            return *this;
        }

        ptrdiff_t uniform_buffer::get_offset(const span<std::byte> & chunk) const {
            return chunk.data() - data.data();
        }

        bool uniform_buffer::is_pristine() const {
            return dirty_range.empty();
        }

        void uniform_buffer::mark_dirty(const math::range<ptrdiff_t> & range) {
            dirty_range.include(range);
        }

        void uniform_buffer::mark_pristine() {
            dirty_range.assign(0, 0);
        }
    }

    namespace gfx
    {
        uniform_registry<opengl::graphics>::uniform_registry(opengl::graphics &) {
            OPENGL_TRY(create_scheme(gfx::uniforms::Color));
            OPENGL_TRY(create_scheme(gfx::uniforms::Model));
            OPENGL_TRY(create_scheme(gfx::uniforms::View));
            OPENGL_TRY(create_scheme(gfx::uniforms::Projection));
        }

        gfx::uniform_scheme<opengl::graphics> & uniform_registry<opengl::graphics>::scheme(const gfx::uniform_layout & layout) {
            auto it = _lookup.find(layout.name);

            if (it != _lookup.end()) {
                auto & uniform = *it->second;
                BOOST_ASSERT_MSG(uniform.layout() == layout, "Found uniform with the same layout name, but the layout is incompatible");

                return uniform;
            }

            return create_scheme(layout);
        }

        gfx::uniform_scheme<opengl::graphics> & uniform_registry<opengl::graphics>::create_scheme(const uniform_layout & layout) {
            auto & scheme = *_pool.construct(++_last_id, layout);
            _lookup.insert_or_assign(layout.name, &scheme);

            return scheme;
        }

        gfx::uniform_scheme<opengl::graphics> * uniform_registry<opengl::graphics>::find_scheme(std::string_view uniform_name) {
            auto it = _lookup.find(uniform_name);
            return it != _lookup.end() ? it->second : nullptr;
        }

        uniform_scheme<opengl::graphics>::uniform_scheme(int id, const gfx::uniform_layout & layout) :
            _id(id),
            _layout(layout),
            _max_buffer_size(static_cast<size_t>(1024 * 1024)) {
            _buffers.emplace_back(_id, _max_buffer_size);
        }

        uniform_data uniform_scheme<opengl::graphics>::instance() {
            if (_free_list.size() > 0) {
                auto entry = _free_list.back();
                _free_list.pop_back();
                return entry;
            }

            auto buffer_offset_alignment = static_cast<size_t>(glGetInteger(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT));
            auto next_offset = aligned_add(_buffers.back().offset, _layout.size, buffer_offset_alignment);

            if (next_offset > _max_buffer_size) {
                _buffers.emplace_back(_id, _max_buffer_size);
                next_offset = aligned_add(0u, _layout.size, buffer_offset_alignment);
            }

            auto & buffer = _buffers.back();

            span<std::byte> chunk { buffer.data.data() + buffer.offset, static_cast<size_t>(_layout.size) };
            buffer.mark_dirty({ static_cast<ptrdiff_t>(buffer.offset), static_cast<ptrdiff_t>(buffer.offset + _layout.size) });

            buffer.offset = next_offset;

            return { static_cast<int>(_buffers.size() - 1), chunk };
        }

        void uniform_scheme<opengl::graphics>::select(const uniform_data & block) {
            if (_current_uniform.chunk == block.chunk) {
                return;
            }

            _current_uniform = block;
            update_current();
        }

        void uniform_scheme<opengl::graphics>::free(const uniform_data & block) {
            if (block.id == -1) {
                return;
            }

            _free_list.push_back(block);
        }

        void uniform_scheme<opengl::graphics>::mark_dirty(const uniform_data & block) {
            opengl::uniform_buffer & buffer = _buffers.at(static_cast<size_t>(block.id));
            buffer.mark_dirty({block.chunk.begin() - buffer.data.data(), block.chunk.end() - buffer.data.data()});
        }

        void uniform_scheme<opengl::graphics>::sync() {
            for (auto & buffer : _buffers) {
                if (buffer.is_pristine()) {
                    continue;
                }

                glBindBuffer(GL_UNIFORM_BUFFER, buffer.handle);
                glBufferSubData(GL_UNIFORM_BUFFER, buffer.dirty_range.min, buffer.dirty_range.size(), buffer.data.data() + buffer.dirty_range.min);
                buffer.mark_pristine();
            }

            update_current();
        }

        void uniform_scheme<opengl::graphics>::update_current() {
            if (_current_uniform.chunk.data()) {
                const opengl::uniform_buffer & buffer = _buffers.at(static_cast<size_t>(_current_uniform.id));
                glBindBufferRange(GL_UNIFORM_BUFFER, static_cast<GLuint>(_id), buffer.handle, buffer.get_offset(_current_uniform.chunk), _current_uniform.chunk.size());
            } else {
                glBindBufferBase(GL_UNIFORM_BUFFER, static_cast<GLuint>(_id), 0);
            }
        }
    }
}

//---------------------------------------------------------------------------
