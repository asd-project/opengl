//---------------------------------------------------------------------------

#include <opengl/mesh.h>
#include <meta/map.h>

#include <fmt/format.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        u32 transform_type(ctti::type_index index) {
            static constexpr auto types = meta::make_map<ctti::type_index, u32, meta::hash_comparator>({
                {ctti::unnamed_type_id<int8_t>(), GL_BYTE},
                {ctti::unnamed_type_id<uint8>(), GL_UNSIGNED_BYTE},
                {ctti::unnamed_type_id<int16_t>(), GL_SHORT},
                {ctti::unnamed_type_id<uint16_t>(), GL_UNSIGNED_SHORT},
                {ctti::unnamed_type_id<int32_t>(), GL_INT},
                {ctti::unnamed_type_id<uint32_t>(), GL_UNSIGNED_INT},
                {ctti::unnamed_type_id<float>(), GL_FLOAT},
                {ctti::unnamed_type_id<double>(), GL_DOUBLE},
            });

            return types[index];
        }

        u32 convert_primitives_type(gfx::primitives_type primitives_type) {
            switch (primitives_type) {
                case gfx::primitives_type::triangles:
                    return GL_TRIANGLES;

                case gfx::primitives_type::lines:
                    return GL_LINES;

                case gfx::primitives_type::triangle_strip:
                    return GL_TRIANGLE_STRIP;

                case gfx::primitives_type::line_strip:
                    return GL_LINE_STRIP;
            }

            return GL_NONE;
        }

        index_params get_index_params(gfx::index_type type) {
            switch (type) {
                case gfx::index_type::u8:
                    return {sizeof(u8), GL_UNSIGNED_BYTE};
                case gfx::index_type::u16:
                    return {sizeof(u16), GL_UNSIGNED_SHORT};
                case gfx::index_type::u32:
                    return {sizeof(u32), GL_UNSIGNED_INT};
            }

            BOOST_ASSERT_MSG(false, "[gfx/mesh] Invalid index type");
            return {};
        }

        i32 primitive_indices_count(u32 primitives_type) {
            switch (primitives_type) {
                case GL_TRIANGLES:
                    return 3;

                case GL_LINES:
                    return 2;

                case GL_TRIANGLE_STRIP: [[ fallthrough ]];
                case GL_LINE_STRIP:
                    return 1;
            }

            BOOST_ASSERT_MSG(false, "[gfx][primitive_indices_count] Invalid primitives type");
            return 1;
        }

        u32 get_cull_face(gfx::polygon_face visible_face) {
            switch (visible_face) {
                case gfx::polygon_face::none:
                    return GL_FRONT_AND_BACK;
                case gfx::polygon_face::back:
                    return GL_FRONT;
                case gfx::polygon_face::front:
                    return GL_BACK;

                case gfx::polygon_face::both:
                default:
                    return GL_NONE;
            }
        }

        basic_mesh::basic_mesh(const gfx::vertex_layout & layout, gfx::primitives_type primitives_type, gfx::polygon_face visible_face) :
            _layout(&layout),
            _primitives_type(opengl::convert_primitives_type(primitives_type)),
            _cull_face(opengl::get_cull_face(visible_face))
        {
            OPENGL_TRY(glGenVertexArrays(1, &_handle));
        }

        basic_mesh::~basic_mesh() {
            if (_handle.valid()) {
                glDeleteVertexArrays(1, &_handle);
            }
        }

        gfx::vertex_buffer<opengl::graphics> & basic_mesh::add_vertex_buffer(const gfx::vertex_layout & layout, const gfx::vertex_data & data) {
            if (!_buffers.empty()) {
                const auto current_vertices_count = static_cast<size_t>(_buffers.front().vertices_count);
                const auto provided_vertices_count = static_cast<size_t>(data.size() / layout.size_in_bytes());

                if (current_vertices_count != provided_vertices_count) {
                    BOOST_THROW_EXCEPTION(std::runtime_error(fmt::format("[opengl::mesh] Vertices count in provided buffer doesn't match existing vertices count ({} vs {})", provided_vertices_count, current_vertices_count)));
                }
            }

            OPENGL_TRY(glBindVertexArray(_handle));

            gfx::vertex_buffer<opengl::graphics> buffer(layout);
            buffer.update(data);

            std::byte * pointer = nullptr;

            GLint attr_count = 0;
            size_t registered_count = 0;
            auto current_attributes = layout.attributes();
            auto all_attributes = _layout->attributes();

            for (int i = 0; i < static_cast<int>(all_attributes.size()); ++i) {
                auto & attribute = all_attributes[i];
                auto count = (attribute.units - 1) / 4 + 1;

                if (std::find(current_attributes.begin(), current_attributes.end(), attribute) == current_attributes.end()) {
                    attr_count += static_cast<GLint>(count);
                    continue;
                }

                if (_registered_attributes.find(i) != _registered_attributes.end()) {
                    glBindVertexArray(0);
                    BOOST_THROW_EXCEPTION(std::runtime_error(std::string("Attribute ") + attribute.name + " is already registered"));
                }

                _registered_attributes.insert(i);
                ++registered_count;

                auto type = opengl::transform_type(attribute.type);
                auto unit_size = attribute.size_in_bytes / attribute.units;

                for (int j = 0; j < count; ++j, ++attr_count) {
                    GLint u = (j == count - 1) ? ((attribute.units - 1) % 4) + 1 : 4;
                    glEnableVertexAttribArray(attr_count);
                    glVertexAttribPointer(attr_count, u, type, attribute.normalize ? GL_TRUE : GL_FALSE, static_cast<GLsizei>(layout.size_in_bytes()), pointer);
                    pointer += u * unit_size;
                }
            }

            glBindVertexArray(0);

            if (registered_count < current_attributes.size()) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Buffer layout is incompatible"));
            }

            return _buffers.emplace_back(std::move(buffer));
        }
    }

    namespace gfx
    {
        mesh<opengl::graphics>::mesh(const mesh_data<opengl::graphics> & data, opengl::graphics & graphics) :
            basic_mesh(data.layout, data.primitives_type, data.visible_face),
            _drawer(opengl::plain_mesh_drawer{}),
            _graphics(&graphics)
        {}

        void mesh<opengl::graphics>::set_indices(const span<const void> & indices, gfx::index_type type) {
            auto v = overloaded{
                [&] (opengl::indexed_mesh_drawer & drawer) {
                    if (!indices.empty()) {
                        drawer.buffer.update(indices, type);
                        return;
                    }

                    glBindVertexArray(_handle);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                    glBindVertexArray(0);

                    _drawer.emplace<opengl::plain_mesh_drawer>();
                },

                [&] (opengl::plain_mesh_drawer &) {
                    if (indices.empty()) {
                        return;
                    }

                    auto & drawer = _drawer.emplace<opengl::indexed_mesh_drawer>();

                    glBindVertexArray(_handle);
                    drawer.buffer.update(indices, type);
                    glBindVertexArray(0);
                }
            };

            std::visit(v, _drawer);
        }

        void mesh<opengl::graphics>::render() {
#if GL_DEBUG
            const auto attributes = _layout->attributes();

            if (_registered_attributes.size() < attributes.size()) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Mesh is incomplete, there are missing layout attributes"));
            }
#endif

            _graphics->set_cull_face(_cull_face);

            std::visit([this](auto & drawer) { drawer.render(*this); }, _drawer);
        }

//---------------------------------------------------------------------------

        vertex_buffer<opengl::graphics>::vertex_buffer(const vertex_layout & layout) : layout(&layout) {
            OPENGL_TRY(glGenBuffers(1, &handle));
        }

        vertex_buffer<opengl::graphics>::~vertex_buffer() {
            if (handle.valid()) {
                glDeleteBuffers(1, &handle);
            }
        }

        void vertex_buffer<opengl::graphics>::update(const gfx::vertex_data & vd) {
            auto data_size = vd.size();

            if (data_size % layout->size_in_bytes() != 0) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Size of vertex data doesn't match vertex input layout"));
            }

            OPENGL_TRY(glBindBuffer(GL_ARRAY_BUFFER, handle));

            vertices_count = static_cast<GLsizei>(data_size / layout->size_in_bytes());

            if (vertices_count > 0) {
                OPENGL_TRY(glBufferData(GL_ARRAY_BUFFER, data_size, vd.data(), GL_STATIC_DRAW));
            }
        }

        void vertex_buffer<opengl::graphics>::update_region(const math::uint_range & region, const gfx::vertex_data & vd) {
            auto data_size = vd.size();

            if (data_size % layout->size_in_bytes() != 0) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Size of vertex data doesn't match vertex input layout"));
            }

            if (static_cast<GLsizei>(region.max / layout->size_in_bytes()) > vertices_count) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Range goes out of buffer boundary"));
            }

            OPENGL_TRY(glBindBuffer(GL_ARRAY_BUFFER, handle));

            if (data_size > 0) {
                OPENGL_TRY(glBufferSubData(GL_ARRAY_BUFFER, region.min, region.size(), vd.data()));
            }
        }
    }

//---------------------------------------------------------------------------

    namespace opengl
    {
        void plain_mesh_drawer::render(opengl::mesh & mesh) {
            OPENGL_CHECK();

            glBindVertexArray(mesh.handle());
            OPENGL_TRY(glDrawArrays(mesh.primitives_type(), static_cast<GLint>(0), mesh.vertices_count()));
            glBindVertexArray(0);
        }

        void indexed_mesh_drawer::render(opengl::mesh & mesh) {
            OPENGL_CHECK();

            glBindVertexArray(mesh.handle());
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.handle);
            OPENGL_TRY(glDrawElements(mesh.primitives_type(), buffer.indices_count, buffer.indices_type, nullptr));
            glBindVertexArray(0);
        }

        index_buffer::index_buffer() {
            glGenBuffers(1, &handle);
        }

        index_buffer::~index_buffer() {
            if (handle.valid()) {
                glDeleteBuffers(1, &handle);
            }
        }

        void index_buffer::update(const span<const void> & indices, gfx::index_type type) {
            auto params = get_index_params(type);

            indices_type = params.type;
            indices_count = static_cast<int32>(indices.size()) / params.size;

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);

            if (indices_count > 0) {
                OPENGL_TRY(glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size(), indices.data(), GL_STATIC_DRAW));
            }
        }
    }
}

//---------------------------------------------------------------------------
