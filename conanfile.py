import os

from conans import ConanFile, CMake

project_name = "opengl"


class OpenglConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Opengl integration for asd"
    settings = "os", "compiler", "build_type", "arch"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "tools*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.gfx/0.0.1@asd/testing",
        "spdlog/[~=1.8]"
    )

    def requirements(self):
        if self.settings.os != "Emscripten":
            self.requires("glew/2.1.0@bincrafters/stable")

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*", dst="tools", src="tools")
        self.copy("*.h", dst="include", src="include")
        self.copy("*.h", dst="generated/include", src="generated/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.includedirs = ["include", "generated/include"]
        self.cpp_info.libs = [project_name]
