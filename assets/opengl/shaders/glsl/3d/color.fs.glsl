/**
 *
 */
#version 330 core

layout(std140) uniform Color
{
    vec4 color;
};

out vec4 fs_color;

void main(void)
{
    fs_color = color;
}
