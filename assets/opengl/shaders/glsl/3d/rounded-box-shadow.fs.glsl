/**
 *
 */
#version 330 core

layout(std140) uniform Color
{
    vec4 color;
};

layout(std140) uniform RoundedBoxShadow
{
    vec2 shadow_blur;
    vec2 corner_radius;
};

in vec2 vs_texcoord;

out vec4 fs_color;

const float pi = 3.141592653589793;

float gaussian(float x, float sigma) {
    return exp(-(x * x) / (2.0 * sigma * sigma)) / (sqrt(2.0 * pi) * sigma);
}

vec2 erf(vec2 x)
{
    vec2 s = sign(x), a = abs(x);
    x = 1.0 + (0.278393 + (0.230389 + 0.078108 * a * a) * a) * a;
    x *= x;
    return s - s / (x * x);
}

// Return the blurred mask along the x dimension
float shadow_x(float x, float y, vec2 half_size) {
    float delta = min(half_size.y - corner_radius.y - abs(y), 0.0);
    float curved = half_size.x - corner_radius.x + sqrt(max(0.0, corner_radius.x * corner_radius.y - delta * delta));
    vec2 integral = 0.5 + 0.5 * erf((x + vec2(-curved, curved)) * (sqrt(2) / shadow_blur));
    return integral.y - integral.x;
}

float shadow(vec2 point)
{
    point -= vec2(0.5);
    vec2 half_size = vec2(0.5) - shadow_blur;

    float low = point.y - half_size.y;
    float high = point.y + half_size.y;
    float start = clamp(-0.5 * shadow_blur.y, low, high);
    float end = clamp(0.5 * shadow_blur.y, low, high);

    float step = (end - start) * 0.2;
    float y = start + step * 0.5;
    float value = 0.0;

    for (int i = 0; i < 5; i++) {
        value += shadow_x(point.x, point.y - y, half_size) * gaussian(y, shadow_blur.y) * step;
        y += step;
    }

    return value;
}

void main(void)
{
    fs_color = vec4(color.rgb, color.a * shadow(vs_texcoord));
}
