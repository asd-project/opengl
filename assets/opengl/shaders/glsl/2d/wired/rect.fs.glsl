/**
*
*/
#version 330 core

layout(std140) uniform BrushOptions
{
    vec4  color;
    float lineWidth;
};

in Vertex
{
    vec2 texcoord;
    vec2 ratio;
} vs;

out vec4 fscolor;

void main(void)
{
    float lx = lineWidth / vs.ratio.x;
    float ly = lineWidth / vs.ratio.y;

    float x = vs.texcoord[0];
    float y = vs.texcoord[1];

    if (x > lx && x < (1.0 - lx) && y > ly && y < (1.0 - ly))
        discard;

    fscolor = color;
}
