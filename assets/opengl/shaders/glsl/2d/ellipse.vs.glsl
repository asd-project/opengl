/**
 *  !vertex: p2 t2
 */
#version 330 core

layout(std140) uniform Area
{
    vec2  pos;
    vec2  size;
    float depth;
};

layout(std140) uniform Viewport
{
    vec2 viewport;
};

in vec2 position;
in vec2 texcoord;

out Vertex
{
    vec2 texcoord;
    vec4 ratio;
} vs;

void main(void)
{
    vs.ratio.w = vs.ratio.z = max(size.x, size.y);
    vs.ratio.xy = vs.ratio.zw / size;
    vs.texcoord = texcoord;

    gl_Position = vec4((position * vs.ratio.zw + pos) * vec2(1.0, viewport.x / viewport.y), depth, 1.0);
}
