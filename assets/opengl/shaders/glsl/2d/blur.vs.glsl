/**
 *  !vertex: p2 t2
 */
#version 330 core

in vec2 position;
in vec2 texcoord;

out vec2 vs_texcoord;

void main(void)
{
    vs_texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}
