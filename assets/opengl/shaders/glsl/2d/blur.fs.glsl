/**
 *
 */
#version 330 core

uniform sampler2D texture0;

layout(std140) uniform Resolution
{
    vec4 resolution;
};

in vec2 vs_texcoord;

out vec4 fscolor;

const int kernel_size = 11;
const int half_size = (kernel_size - 1) / 2;
const float sigma = 3.0;

float normpdf(in float x)
{
    return 0.39894 * exp(-0.5 * x * x / (sigma * sigma)) / sigma;
}

void main(void)
{
    vec4 input_color = texture(texture0, vs_texcoord);

    float kernel[kernel_size];
    vec3 final_colour = vec3(0.0);

    kernel[half_size] = normpdf(0.0);
    float Z = kernel[half_size];

    for (int j = 1; j <= half_size; ++j) {
        kernel[half_size + j] = kernel[half_size - j] = normpdf(float(j));
        Z += 2.0 * kernel[half_size + j];
    }

    for (int i = -half_size; i <= half_size; ++i) {
        for (int j = -half_size; j <= half_size; ++j) {
            final_colour += kernel[half_size + j] * kernel[half_size + i] * texture(texture0, vs_texcoord + vec2(float(i), float(j)) * resolution.xy).rgb;
        }
    }

    fscolor = vec4(mix(input_color.rgb, final_colour / (Z * Z), 0.75), input_color.a);
}
