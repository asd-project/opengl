/**
 *  !vertex: p2 t2
 */
#version 330 core

in vec2 position;
in vec2 texcoord;

out Vertex
{
    vec2 texcoord;
} vs;

void main(void)
{
    vs.texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}
