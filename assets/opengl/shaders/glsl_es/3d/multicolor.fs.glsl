#version 300 es
precision highp float;

in Vertex
{
    vec4 color;
} vtx;

out vec4 fscolor;

void main(void)
{
    fscolor = vtx.color;
}
