#version 300 es
precision highp float;

uniform sampler2D texture0;

layout(std140) uniform Resolution
{
    vec4 resolution;
};

in vec2 vs_texcoord;

out vec4 fscolor;

const int kernelSize = 11;
const int halfSize = (kernelSize - 1) / 2;
const float sigma = 3.0;

float normpdf(in float x)
{
    return 0.39894 * exp(-0.5 * x * x / (sigma * sigma)) / sigma;
}

void main(void)
{
    vec4 inputColor = texture(texture0, vs_texcoord);

    vec3 c = inputColor.rgb;

    float kernel[kernelSize];
    vec3 final_colour = vec3(0.0);

    float Z = 0.0;

    for (float j = 0.0; j <= halfSize; j += 1.0) {
        kernel[int(halfSize + j)] = kernel[int(halfSize - j)] = normpdf(j);
    }

    for (int j = 0; j < kernelSize; j += 1) {
        Z += kernel[j];
    }

    for (float i = -halfSize; i <= halfSize; i += 1.0) {
        for (float j = -halfSize; j <= halfSize; j += 1.0) {
            final_colour += kernel[int(halfSize + j)] * kernel[int(halfSize + i)] * texture(texture0, vs_texcoord + vec2(i, j) * resolution.xy).rgb;
        }
    }

    fscolor = mix(inputColor, vec4(final_colour / (Z * Z), 1.0), 0.75);
}
