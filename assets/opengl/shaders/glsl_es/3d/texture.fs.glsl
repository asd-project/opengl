#version 300 es
precision highp float;

uniform sampler2D texture0;

in Vertex
{
    vec2 texcoord;
} vtx;

out vec4 fscolor;

void main(void)
{
    fscolor = texture(texture0, vtx.texcoord);
}
