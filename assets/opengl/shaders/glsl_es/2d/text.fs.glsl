#version 300 es
precision highp float;

layout(std140) uniform Color
{
    vec4 color;
};

uniform sampler2D texture0;

in Vertex
{
    vec2 texcoord;
} vtx;

out vec4 fscolor;

void main(void)
{
    fscolor = vec4(color[0], color[1], color[2], texture(texture0, vtx.texcoord)[0] * color[3]);
}
