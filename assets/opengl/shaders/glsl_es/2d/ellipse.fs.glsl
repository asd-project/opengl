#version 300 es
precision highp float;

layout(std140) uniform Color
{
    vec4 color;
};

in Vertex
{
    vec2 texcoord;
    vec4 ratio;
} vs;

out vec4 fscolor;

void main(void)
{
    float x = (vs.texcoord[0] * 2.0 - 1.0) * vs.ratio.x;
    float y = (vs.texcoord[1] * 2.0 - 1.0) * vs.ratio.y;

    if (x * x + y * y > 1.0) {
        discard;
    }

    fscolor = color;
}
