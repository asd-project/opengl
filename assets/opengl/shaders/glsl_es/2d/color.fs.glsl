#version 300 es
precision highp float;

layout(std140) uniform Color
{
    vec4 color;
};

out vec4 fscolor;

void main(void)
{
    fscolor = color;
}
