//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/texture.h>
#include <opengl/opengl.h>
#include <boost/container/stable_vector.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        using texture_id = gfx::object_id<class texture_tag>;

        template <>
        class texture<opengl::graphics>
        {
        public:
            using graphics_type = opengl::graphics;

            texture(const gfx::texture_data<opengl::graphics> & data, GLenum target = GL_TEXTURE_2D);

            texture(texture && t) noexcept :
                _id(std::move(t._id)),
                _target(t._target),
                _data(std::move(t._data))
                {}

            texture & operator = (texture && t) noexcept {
                std::swap(_id, t._id);
                std::swap(_target, t._target);
                std::swap(_data, t._data);

                return *this;
            }

            ~texture();

            gfx::texture_id id() const {
                return _id;
            }

            GLenum target() const {
                return _target;
            }

            math::uint_size size() const {
                return _data.size;
            }

            size_t size_in_bytes() const {
                return static_cast<size_t>(_data.size.area()) * bytes_per_pixel(_data.format);
            }

            gfx::texture_format format() const {
                return _data.format;
            }

            auto bpp() const {
                return bytes_per_pixel(_data.format);
            }

            void bind(int slot) {
                glActiveTexture(GL_TEXTURE0 + slot);
                glBindTexture(_target, _id);
            }

            void update(const gfx::texture_data<opengl::graphics> & data);
            void update_pixels(pixel_data data);
            void update_region(const math::uint_rect & region, pixel_data data);
            void set_filtering(texture_filtering filtering);
            void resize(const math::uint_size & size, area_snapping snapping = {section_snapping::start, section_snapping::start});

        private:
            gfx::texture_id _id;
            GLenum _target;
            gfx::texture_data<opengl::graphics> _data;
        };
    }
}
