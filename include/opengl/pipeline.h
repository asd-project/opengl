//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/pipeline.h>
#include <opengl/shader.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template<>
        class pipeline<opengl::graphics>
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            pipeline(const gfx::pipeline_data<opengl::graphics> & data);

            export_api(opengl)
            void activate();

        private:
            gfx::shader_program<opengl::graphics> & _program;
#if !ASD_OPENGL_ES
            GLenum _polygon_mode;
#endif
        };
    }

    namespace opengl
    {
        using pipeline = gfx::pipeline<opengl::graphics>;
        using pipeline_registry = gfx::pipeline_registry<opengl::graphics>;
    }
}
