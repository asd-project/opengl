//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <boost/predef.h>

#include <color/color.h>
#include <gfx/uniform.h>
#include <gfx/mesh.h>
#include <gfx/render_pass.h>

#include <boost/preprocessor/stringize.hpp>

//---------------------------------------------------------------------------

#if BOOST_OS_LINUX
    #define GL_GLEXT_PROTOTYPES
    #define GLX_GLXEXT_PROTOTYPES
#endif

#include <GL/glew.h>

#if ASD_DEBUG
    #define GL_DEBUG 1
#else
    #define GL_DEBUG 0
#endif

#if ASD_PLAT_EMSCRIPTEN || BOOST_PLAT_ANDROID || BOOST_OS_IOS
    #define ASD_OPENGL_ES 1
#else
    #define ASD_OPENGL_ES 0
#endif

#undef major
#undef minor

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        enum class profile
        {
            core,
            compatibility,
            es
        };

        struct configuration
        {
            configuration() {}

            configuration(opengl::profile profile, int major, int minor, int flags)
                : profile(profile), major(major), minor(minor), flags(flags) {}

            configuration(const configuration & a) = default;

#if ASD_OPENGL_ES
            opengl::profile profile = opengl::profile::es;
            int major = 3;
            int minor = 0;
            int flags = 0;
#else
            opengl::profile profile = opengl::profile::core;
            int major = 3;
            int minor = 3;
            int flags = 0;
#endif
        };

        class graphics
        {
        public:
            static constexpr gfx::matrix_order matrix_order = gfx::matrix_order::column_major;

            explicit graphics(const opengl::configuration & config) : _config(config) {}

            graphics(const opengl::graphics &) = default;

            export_api(opengl)
            void set_cull_face(GLenum cull_face);

            export_api(opengl)
            void set_blending(gfx::blending blending);

            export_api(opengl)
            static void check_for_errors(const char * context);

            static std::string create_version_string(const configuration & config) {
                return "#version " + std::to_string(config.major) + std::to_string(config.minor) + "0 " +
                    (config.profile == opengl::profile::es ? "es" : "core");
            }

        private:
            opengl::configuration _config;
            GLenum _cull_face = GL_NONE;
            gfx::blending _blending = gfx::blending::none;
        };

        export_api(opengl)
        void GLAPIENTRY glDebugCallbackFunc(uint32 source, uint32 type, uint32 id, uint32 severity, int length, const char * message, const void * userParam);
    }

    //---------------------------------------------------------------------------

    inline GLint glGetInteger(GLenum param) {
        GLint value;
        glGetIntegerv(param, &value);
        return value;
    }
}

// #define LOG_OPENGL_CALLS

#if GL_DEBUG
// #ifdef LOG_OPENGL_CALLS
// #define OPENGL_TRY(x) spdlog::debug(#x); x; asd::opengl::graphics::check_for_errors(__FILE__ ":" BOOST_PP_STRINGIZE(__LINE__) "\n" #x)
// #else
// #define OPENGL_TRY(x) x
#define OPENGL_CHECK() asd::opengl::graphics::check_for_errors(__FILE__ ":" BOOST_PP_STRINGIZE(__LINE__))
#define OPENGL_TRY(x) x; asd::opengl::graphics::check_for_errors(__FILE__ ":" BOOST_PP_STRINGIZE(__LINE__) "\n" #x)
#define OPENGL_TRY_AND_RETURN(x) [&]() { auto && _ = x; asd::opengl::graphics::check_for_errors(__FILE__ ":" BOOST_PP_STRINGIZE(__LINE__) "\n" #x); return _; }()
// #endif
#else
#define OPENGL_CHECK()
#define OPENGL_TRY(x) x
#define OPENGL_TRY_AND_RETURN(x) x
#endif
