//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <variant>
#include <container/set.h>
#include <boost/container/stable_vector.hpp>

#include <opengl/opengl.h>
#include <gfx/mesh.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        using buffer_id = gfx::object_id<class buffer_tag>;
        using mesh_id = gfx::object_id<class mesh_tag>;

        struct index_params
        {
            u8 size;
            u32 type;
        };

        export_api(opengl)
        u32 transform_type(ctti::type_index index);

        export_api(opengl)
        u32 transform_primitives_type(gfx::primitives_type primitives_type);

        export_api(opengl)
        index_params get_index_params(gfx::index_type type);

        export_api(opengl)
        i32 primitive_indices_count(u32 primitives_type);
    }

    namespace gfx
    {
        template<>
        class vertex_buffer<opengl::graphics>
        {
        public:
            export_api(opengl)
            vertex_buffer(const gfx::vertex_layout & layout);
            vertex_buffer(vertex_buffer &&) noexcept = default;
            export_api(opengl)
            ~vertex_buffer();

            vertex_buffer & operator = (vertex_buffer && m) noexcept = default;

            export_api(opengl)
            void update(const gfx::vertex_data & vd);

            export_api(opengl)
            void update_region(const math::uint_range & region, const gfx::vertex_data & vd);

            template <class T> requires (!std::is_same_v<plain<T>, void>)
            void update_region(const math::uint_range & region, const span<T> & vd) {
                update_region(region * sizeof(T), gfx::vertex_data(vd));
            }

            opengl::buffer_id handle;
            i32 vertices_count = 0;
            const gfx::vertex_layout * layout;
        };
    }

    namespace opengl
    {
        struct index_buffer
        {
            export_api(opengl)
            index_buffer();
            index_buffer(index_buffer &&) noexcept = default;

            export_api(opengl)
            ~index_buffer();

            index_buffer & operator = (index_buffer && m) noexcept = default;

            export_api(opengl)
            void update(const span<const void> & indices, gfx::index_type type);

            export_api(opengl)
            void update(math::uint_range & range, const span<const void> & indices, gfx::index_type type);

            opengl::buffer_id handle;
            i32 indices_count = 0;
            u32 indices_type = 0;
        };

        class plain_mesh_drawer
        {
        public:
            export_api(opengl)
            void render(gfx::mesh<opengl::graphics> & m);
        };

        class indexed_mesh_drawer
        {
        public:
            indexed_mesh_drawer() noexcept = default;
            indexed_mesh_drawer(indexed_mesh_drawer &&) noexcept = default;

            indexed_mesh_drawer & operator = (indexed_mesh_drawer && m) noexcept = default;

            export_api(opengl)
            void render(gfx::mesh<opengl::graphics> & m);

            index_buffer buffer;
        };

        class basic_mesh
        {
        public:
            export_api(opengl)
            basic_mesh(const gfx::vertex_layout & layout, gfx::primitives_type primitives_type, gfx::polygon_face visible_face);

            basic_mesh(basic_mesh && m) noexcept = default;

            export_api(opengl)
            virtual ~basic_mesh();

            basic_mesh & operator = (basic_mesh && m) noexcept = default;

            u32 handle() const {
                return _handle;
            }

            u32 primitives_type() const {
                return _primitives_type;
            }

            i32 vertices_count() const {
                return _buffers.empty() ? 0 : _buffers.front().vertices_count;
            }

            export_api(opengl)
            gfx::vertex_buffer<opengl::graphics> & add_vertex_buffer(const gfx::vertex_layout & layout, const gfx::vertex_data & data = {});

        protected:
            const gfx::vertex_layout * _layout;
            opengl::mesh_id _handle;
            u32 _primitives_type = 0;
            u32 _cull_face = 0;
            boost::container::stable_vector<gfx::vertex_buffer<opengl::graphics>> _buffers;

            set<int> _registered_attributes;
        };

    }

    namespace gfx
    {
        template<>
        class mesh<opengl::graphics> : public opengl::basic_mesh
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            mesh(const mesh_data<opengl::graphics> & data, opengl::graphics & graphics);

            mesh(mesh &&) noexcept = default;

            mesh & operator = (mesh && m) noexcept = default;

            export_api(opengl)
            void set_indices(const span<const void> & indices, gfx::index_type type);

            void set_indices(const span<const u8> & indices) {
                set_indices(span<const void>{indices}, gfx::index_type::u8);
            }

            void set_indices(const span<const u16> & indices) {
                set_indices(span<const void>{indices}, gfx::index_type::u16);
            }

            void set_indices(const span<const u32> & indices) {
                set_indices(span<const void>{indices}, gfx::index_type::u32);
            }

            int primitives_count() const {
                auto v = overloaded{
                    [&] (const opengl::indexed_mesh_drawer & drawer) {
                        return drawer.buffer.indices_count;
                    },

                    [&] (const opengl::plain_mesh_drawer &) {
                        return vertices_count();
                    }
                };

                return std::visit(v, _drawer) / opengl::primitive_indices_count(_primitives_type);
            }

            export_api(opengl)
            void render();

        private:
            std::variant<opengl::plain_mesh_drawer, opengl::indexed_mesh_drawer> _drawer;
            opengl::graphics * _graphics;
        };

        template <>
        class registry<gfx::mesh<opengl::graphics>> :
            public basic_registry<registry<gfx::mesh<opengl::graphics>>, gfx::mesh<opengl::graphics>>
        {
        public:
            registry(opengl::graphics & graphics) :
                _graphics(graphics) {}

            auto construct(void * location, init_data<object_type> && data) {
                return new (location) object_type(data, _graphics);
            }

        private:
            opengl::graphics & _graphics;
        };
    }

    namespace opengl
    {
        using mesh = gfx::mesh<opengl::graphics>;
        using mesh_registry = gfx::mesh_registry<opengl::graphics>;
    }
}
