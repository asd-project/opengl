//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/render_pass.h>

#include <opengl/opengl.h>
#include <opengl/render_target.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <>
        class render_pass<opengl::graphics>
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            render_pass(const gfx::render_pass_data<opengl::graphics> & data, opengl::graphics & graphics);

            export_api(opengl)
            void start();

            export_api(opengl)
            void end();

            void enable_feature(gfx::feature feature) {
                _data.features.set(feature);
            }

            void disable_feature(gfx::feature feature) {
                _data.features.clear(feature);
            }

        private:
            gfx::render_pass_data<opengl::graphics> _data;
            opengl::graphics * _graphics;
        };

        template <>
        class registry<gfx::render_pass<opengl::graphics>> :
            public basic_registry<registry<gfx::render_pass<opengl::graphics>>, gfx::render_pass<opengl::graphics>>
        {
        public:
            registry(opengl::graphics & graphics) :
                _graphics(graphics) {}

            auto construct(void * location, init_data<object_type> && data) {
                return new (location) object_type(data, _graphics);
            }

        private:
            opengl::graphics & _graphics;
        };
    }

    namespace opengl
    {
        using render_pass = gfx::render_pass<opengl::graphics>;
        using render_pass_registry = gfx::render_pass_registry<opengl::graphics>;
    }
}
