//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/uniform.h>
#include <gfx/registry.h>

#include <opengl/opengl.h>

#include <boost/container/stable_vector.hpp>

#include <meta/tuple/enumerate.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        using gfx::uniform_layout_element;
        using gfx::uniform_layout;
        using gfx::uniform_data;

        struct uniform_buffer
        {
            export_api(opengl)
            uniform_buffer(int idx, size_t max_size);

            uniform_buffer(uniform_buffer && buffer) noexcept;

            export_api(opengl)
            ~uniform_buffer();

            uniform_buffer & operator = (uniform_buffer && buffer) noexcept;

            export_api(opengl)
            ptrdiff_t get_offset(const span<std::byte> & data) const;

            bool is_pristine() const;

            void mark_dirty(const math::range<ptrdiff_t> & range);

            void mark_pristine();

            GLuint handle = 0;
            size_t offset = 0;
            std::vector<std::byte> data;
            math::range<ptrdiff_t> dirty_range;
        };
    }

    namespace gfx
    {
        template<>
        class uniform_scheme<opengl::graphics>
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            uniform_scheme(int id, const uniform_layout & layout);

            uniform_scheme(uniform_scheme &&) noexcept = default;

            int id() const {
                return _id;
            }

            const gfx::uniform_layout & layout() const {
                return _layout;
            }

            const gfx::uniform_layout_element & at(size_t idx) const {
                return _layout.at(idx);
            }

            const gfx::uniform_layout_element & operator[](size_t idx) const {
                return _layout[idx];
            }

            template <gfx::buffer_element_type ... T>
            uniform<opengl::graphics, T...> instance() {
                check_elements(meta::types_v<T...>);
                return { *this, this->instance() };
            }

            template <gfx::buffer_element_type ... T>
            uniform<opengl::graphics, T...> instance(const T &... args) {
                check_elements(meta::types_v<T...>);

                uniform<opengl::graphics, T...> block{ *this, this->instance() };
                block.assign(args...);

                return block;
            }

            export_api(opengl)
            void select(const uniform_data & block);

            export_api(opengl)
            void mark_dirty(const uniform_data & block);

            export_api(opengl)
            void free(const uniform_data & block);

            export_api(opengl)
            void sync();

        private:
            export_api(opengl)
            uniform_data instance();

            void update_current();

            template <class Types>
            void check_elements(Types types) {
#if GL_DEBUG
                BOOST_ASSERT_MSG(_layout.elements_count() == Types::count, "Numbers of elements of the uniform layout and requested uniform block don't match");

                meta::enumerate(types, [this](auto index, auto type) {
                    using Type = typename decltype(type)::type;
                    using Raw = typename raw_data<plain<Type>>::type;
                    BOOST_ASSERT_MSG(_layout.at(index).type == ctti::unnamed_type_id<Raw>(), "Type of the requested uniform block element must be equal to the type of the uniform element at the given offset");
                });
#endif
            }

            int _id;
            const uniform_layout & _layout;
            boost::container::stable_vector<opengl::uniform_buffer> _buffers;
            std::vector<uniform_data> _free_list;
            size_t _max_buffer_size;
            uniform_data _current_uniform;
        };

        template<>
        class uniform_registry<opengl::graphics>
        {
        public:
            export_api(opengl)
            uniform_registry(opengl::graphics &);

            export_api(opengl)
            gfx::uniform_scheme<opengl::graphics> & scheme(const uniform_layout & layout);

            export_api(opengl)
            gfx::uniform_scheme<opengl::graphics> * find_scheme(std::string_view uniform_name);

            template <gfx::buffer_element_type ... T>
            gfx::uniform_factory<opengl::graphics, T...> factory(const gfx::static_uniform_layout_storage<T...> & layout) {
                return {scheme(layout)};
            }

            template <gfx::buffer_element_type ... T>
            gfx::uniform<opengl::graphics, T...> create(const gfx::uniform_layout & layout) {
                return scheme(layout).template instance<T...>();
            }

            template <gfx::buffer_element_type ... T>
            gfx::uniform<opengl::graphics, T...> create(const gfx::static_uniform_layout_storage<T...> & layout) {
                return scheme(layout).template instance<T...>();
            }

            template <gfx::buffer_element_type ... T>
            gfx::uniform<opengl::graphics, T...> create(const gfx::static_uniform_layout_storage<T...> & layout, const T & ... args) {
                return scheme(layout).template instance<T...>(args...);
            }

        private:
            gfx::uniform_scheme<opengl::graphics> & create_scheme(const uniform_layout & layout);

            int _last_id = 0;
            boost::object_pool<gfx::uniform_scheme<opengl::graphics>> _pool;
            asd::map<std::string_view, gfx::uniform_scheme<opengl::graphics> *> _lookup;
        };
    }

    namespace opengl
    {
        using uniform_scheme = gfx::uniform_scheme<opengl::graphics>;
        using uniform_registry = gfx::uniform_registry<opengl::graphics>;

        template <class ... T>
        using uniform = gfx::uniform<opengl::graphics, T...>;
    }
}
