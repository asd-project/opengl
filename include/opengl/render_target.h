//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/render_target.h>
#include <opengl/opengl.h>
#include <opengl/texture.h>
#include <boost/container/stable_vector.hpp>

#include <meta/unique_value.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <>
        class render_target<opengl::graphics>
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            render_target(const render_target_data<opengl::graphics> & data);

            render_target(render_target && t) = default;

            export_api(opengl)
            ~render_target();

            export_api(opengl)
            render_target & operator = (render_target && t);

            export_api(opengl)
            void resize(const math::uint_size & size);

            export_api(opengl)
            gfx::texture<opengl::graphics> & add_buffer(const render_target_buffer_data<opengl::graphics> & data);

            export_api(opengl)
            gfx::texture<opengl::graphics> & get_buffer(int32 index);

            export_api(opengl)
            GLuint framebuffer() const;

        private:
            export_api(opengl)
            void update_size();

            meta::unique_value<GLuint> _framebuffer;
            std::vector<GLenum> _attachments;
            boost::container::stable_vector<gfx::texture<opengl::graphics>> _color_channels;
            meta::unique_value<GLuint> _depth_channel;
            math::uint_size _size;
        };
    }

    namespace opengl
    {
        using render_target = gfx::render_target<opengl::graphics>;
        using render_target_registry = gfx::render_target_registry<opengl::graphics>;
    }
}
