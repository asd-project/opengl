//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <opengl/mesh.h>
#include <gfx/multimesh.h>

#include <boost/container/stable_vector.hpp>

#include <meta/tuple/enumerate.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace opengl
    {
        using gfx::multimesh_data;

        struct multimesh_buffer
        {
            export_api(opengl)
            multimesh_buffer(size_t max_size);

            multimesh_buffer(multimesh_buffer &&) noexcept = default;

            export_api(opengl)
            ~multimesh_buffer();

            multimesh_buffer & operator = (multimesh_buffer &&) noexcept = default;

            export_api(opengl)
            ptrdiff_t get_offset(const span<std::byte> & chunk) const;

            GLuint handle = 0;
            size_t offset = 0;
            std::vector<std::byte> data;
        };

        class plain_multimesh_drawer
        {
        public:
            plain_multimesh_drawer() noexcept = default;
            plain_multimesh_drawer(plain_multimesh_drawer &&) noexcept = default;

            export_api(opengl)
            void render(gfx::multimesh_scheme<opengl::graphics> & m);
        };

        class indexed_multimesh_drawer
        {
        public:
            indexed_multimesh_drawer() noexcept = default;
            indexed_multimesh_drawer(indexed_multimesh_drawer && drawer) noexcept = default;

            export_api(opengl)
            void render(gfx::multimesh_scheme<opengl::graphics> & m);

            index_buffer buffer;
        };
    }

    namespace gfx
    {
        template <>
        class multimesh_scheme<opengl::graphics> : public opengl::basic_mesh
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            multimesh_scheme(const gfx::multimesh_scheme_data<opengl::graphics> & data);

            export_api(opengl)
            multimesh_scheme(multimesh_scheme && m) noexcept = default;

            size_t block_size() const {
                return _instance_layout->size_in_bytes();
            }

            size_t attributes_count() const {
                const auto attributes = _instance_layout->attributes();
                return attributes.size();
            }

            const vertex_attribute & at(size_t idx) const {
                const auto attributes = _instance_layout->attributes();
                return attributes.at(idx);
            }

            const vertex_attribute & operator[](size_t idx) const {
                const auto attributes = _instance_layout->attributes();
                return attributes[idx];
            }

            GLsizei instances_count() const {
                return _instances_count;
            }

            export_api(opengl)
            void free(const multimesh_data & instance);

            export_api(opengl)
            void set_indices(const span<const void> & indices, gfx::index_type type);

            void set_indices(const span<const u8> & indices) {
                set_indices(span<const void>{indices}, index_type::u8);
            }

            void set_indices(const span<const u16> & indices) {
                set_indices(span<const void>{indices}, index_type::u16);
            }

            void set_indices(const span<const u32> & indices) {
                set_indices(span<const void>{indices}, index_type::u32);
            }

            export_api(opengl)
            void render();

            template <class ... T>
            gfx::multimesh<opengl::graphics, T...> instance() {
                check_attributes(meta::types_v<T...>);
                return{ *this, instance() };
            }

            template <class ... T>
            gfx::multimesh<opengl::graphics, T...> instance(const T &... args) {
                check_attributes(meta::types_v<T...>);

                multimesh<opengl::graphics, T...> block{ *this, instance() };
                block.assign(args...);

                return block;
            }

            template <class T>
            void write_attribute(const multimesh_data & instance, ptrdiff_t offset, const T & value) {
                auto ptr = map_attribute(instance, offset);
                ::memcpy(ptr, &value, sizeof(T));
            }

            template <class T>
            T & read_attribute(const multimesh_data & instance, ptrdiff_t offset) {
                return *static_cast<T *>(map_attribute(instance, offset));
            }

            void mark_dirty() {
                _dirty = true;
            }

        private:
            template <class Types>
            void check_attributes(Types types) {
#if GL_DEBUG
                const auto attributes = _instance_layout->attributes();
                BOOST_ASSERT_MSG(attributes.size() == Types::count, "Numbers of attributes of the instance layout and requested uniform block don't match");

                meta::enumerate(types, [&](auto index, auto type) {
                    using Type = typename decltype(type)::type;
                    using Raw = typename raw_data<plain<Type>>::type;

                    auto & attribute = attributes.at(index);
                    constexpr auto actual_type = ctti::unnamed_type_id<typename Raw::value_type>();

                    BOOST_ASSERT_MSG(attribute.type == actual_type, "Type of the requested mesh instance attribute must be equal to the type of the mesh instance attribute at the given offset");
                    BOOST_ASSERT_MSG(attribute.size_in_bytes == sizeof(Raw), "Size of the requested mesh instance attribute must be equal to the size of the mesh instance attribute at the given offset");
                });
#endif
            }

            multimesh_data instance();

            void sync();

            void * map_attribute(const multimesh_data & instance, ptrdiff_t offset);

            const gfx::vertex_layout * _instance_layout;
            bool _dirty = false;

            opengl::multimesh_buffer _instance_buffer;
            std::vector<multimesh_data> _free_list;

            GLsizei _instances_count = 0;
            std::variant<opengl::plain_multimesh_drawer, opengl::indexed_multimesh_drawer> _drawer;
        };
    }

    namespace opengl
    {
        using multimesh = gfx::multimesh_scheme<opengl::graphics>;
        using multimesh_registry = gfx::multimesh_registry<opengl::graphics>;
    }
}
