//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <opengl/opengl.h>
#include <opengl/vertex_layout.h>

#include <gfx/shader.h>
#include <boost/pool/object_pool.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Gfx>
        class uniform_registry;

        template <class Gfx>
        class uniform_scheme;

        struct shader_reflection
        {
            GLuint _program_id;
            const vertex_layout & _layout;
        };

        template <>
        class shader_program<opengl::graphics>
        {
        public:
            using graphics_type = opengl::graphics;

            export_api(opengl)
            shader_program(std::string_view name, const gfx::shader_code & code, gfx::uniform_registry<opengl::graphics> & uniforms);

            export_api(opengl)
            shader_program(shader_program && program) noexcept;

            shader_program & operator = (shader_program && program) = delete;

            export_api(opengl)
            ~shader_program();

            const vertex_layout & layout() const {
                return *_layout;
            }

            export_api(opengl)
            void activate();

        private:
            GLuint _program_id;
            const vertex_layout * _layout;
            std::vector<gfx::uniform_scheme<opengl::graphics> *> _uniforms;
        };

        template <>
        class registry<gfx::shader_program<opengl::graphics>> :
            public basic_registry<registry<gfx::shader_program<opengl::graphics>>, gfx::shader_program<opengl::graphics>>
        {
        public:
            registry(gfx::uniform_registry<opengl::graphics> & uniforms) :
                _uniforms(uniforms) {}

            auto construct(void * location, init_data<object_type> && data) {
                return new (location) object_type(std::move(data.name), std::move(data.code), _uniforms);
            }

        private:
            gfx::uniform_registry<opengl::graphics> & _uniforms;
        };
    }

    namespace opengl
    {
        using shader_program = gfx::shader_program<opengl::graphics>;
        using shader_registry = gfx::shader_registry<opengl::graphics>;
    }
}
